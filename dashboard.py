import streamlit as st 
import plotly.express as ex 
import pandas as pd 
import os
import warnings 
warnings.filterwarnings('ignore')

st.set_page_config(page_title="Superstore",
                   page_icon=":bar_chart:",
                   layout="wide")

# Setup top of page
st.title(":bar_chart: Superstore EDA")
st.markdown('<style>div.block-container{padding-top:1rem;}</style>', unsafe_allow_html=True)

# Import a data file inside the page
fl = st.file_uploader(":file_folder: Upload a file",
                      type=(["csv",
                             "txt",
                             "xlsx",
                             "xls",
                             "json"]))
if fl is not None:
    filename = fl.name
    st.write(filename)
    if fl.type == "csv" or fl.type == "txt":
        df = pd.read_csv(filename)
    elif fl.type == "xls" or fl.type == "xlsx":
        df = pd.read_excel(filename)
    elif fl.type == "json":
        df = pd.read_json(filename)
    else:
        print("Error while uploading. File format not supported. Using dummy file data.")
        df = pd.read_csv("dummy.csv")
else:
    print("File not uploaded. Using dummy data file.")
    df = pd.read_csv("dummy.csv")

# Creation of display columns
first_column, second_column = st.columns(2)
df["Order Date"] = pd.to_datetime(df["Order Date"])

# Selecting the min and max from the column
start_date = pd.to_datetime(df["Order Date"]).min()
end_date = pd.to_datetime(df["Order Date"]).max()

with first_column:
    date1 = pd.to_datetime(st.date_input("Start Date",start_date))

with second_column:
    date2 = pd.to_datetime(st.date_input("End Date"), end_date)

df = df[]









